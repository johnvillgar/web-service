﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CENErrWs
/// </summary>
public class CENErrWs
{
    private short tipoErr;// tipo de error 
    private int codigoErr; // codigo de error 
    private string descripcionErr; // descripcion de error 

    public short TipoErr { get => tipoErr; set => tipoErr = value; }
    public int CodigoErr { get => codigoErr; set => codigoErr = value; }
    public string DescripcionErr { get => descripcionErr; set => descripcionErr = value; }
}