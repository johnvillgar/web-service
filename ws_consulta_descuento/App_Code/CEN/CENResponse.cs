﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CENResponse
/// </summary>
public class CENResponseDescuento
{
    private CENPorDescTarj descuento;
    private CENErrWs errws;

    public CENPorDescTarj Descuento { get => descuento; set => descuento = value; }
    public CENErrWs Errws { get => errws; set => errws = value; }
}

public class CENResponseCategoria
{
    private int respuesta;
    private string descripcion;

    public int Respuesta { get => respuesta; set => respuesta = value; }
    public string Descripcion { get => descripcion; set => descripcion = value; }

    public CENResponseCategoria(){
     this.descripcion = "NO SE ENCONTRO REGISTRO..";
    }

}

public class CENResponseCategoria2
{
    public CENResponseCategoria categoria { get; set; }
    public CENErrWs Errws { get; set; }
    
}


public class CENResponsexmlDescuentos
{
    public string Descuento { get; set; }
    public CENErrWs Errws { get; set; }

}

public class CENResponseCantDescuentoxListaCategoria
{
    public Int16 cantDsto { get; set; }
    public CENErrWs Errws { get; set; }
}





