﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CENPorDescTarj
/// </summary>
public class CENPorDescTarj
{
    private int codigo;
    private string descripcion;
    private decimal porcentaje;

    public CENPorDescTarj()
    {
        this.descripcion = "NO SE ENCONTRO REGISTRO..";
    }

    public int Codigo { get => codigo; set => codigo = value; }
    public string Descripcion { get => descripcion; set => descripcion = value; }
    public decimal Porcentaje { get => porcentaje; set => porcentaje = value; }


}