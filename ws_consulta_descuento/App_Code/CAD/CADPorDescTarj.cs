﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;

/// <summary>
/// Descripción breve de CADPorDescTarj
/// </summary>
public class CADPorDescTarj
{
    private MySqlConnection conexion;
    public CENPorDescTarj consultarDescuento(string CodigoBin, string categoria, string Fecha, string hora)
    {

        CENPorDescTarj CENPorDescTarj = new CENPorDescTarj();
        MySqlDataReader dr;
        CADConexion conex = new CADConexion();


        try
        {
            DateTime fech = DateTime.Parse(Fecha);
            Fecha = fech.ToString("yyyy-MM-dd");
        
            
            using (conexion = new MySqlConnection(conex.CxSQL()))
            {
                conex.OpenConection(conexion);
                using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_obtener_descuentoTarjeta", conexion))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add (new MySqlParameter("p_codigo_BIN",MySqlDbType.VarChar)).Value= CodigoBin;
                    command.Parameters.Add(new MySqlParameter("p_categoria", MySqlDbType.Int32)).Value = categoria;
                    command.Parameters.Add(new MySqlParameter("p_fecha", MySqlDbType.VarChar)).Value = Fecha;
                    command.Parameters.Add(new MySqlParameter("p_hora", MySqlDbType.VarChar)).Value = hora;
                    command.CommandTimeout = 0;
                    dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CENPorDescTarj.Codigo = Convert.ToInt32(dr["CODIGO"].ToString());
                            CENPorDescTarj.Descripcion = dr["DESCRIPCION"].ToString().Trim();
                            CENPorDescTarj.Porcentaje = Convert.ToDecimal(dr["PORCENTAJE"].ToString());
                        }
                    }
                }

            }
            return CENPorDescTarj;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conex.CerrarConexion(conexion);
        }
    }

    public CENPorDescTarj CADListaDescuentoxListCateg(string xmlCategoria, string Fecha, string hora)
    {

        CENPorDescTarj CENPorDescTarj = new CENPorDescTarj();
        MySqlDataReader dr;
        CADConexion conex = new CADConexion();


        try
        {
            DateTime fech = DateTime.Parse(Fecha);
            Fecha = fech.ToString("yyyy-MM-dd");


            using (conexion = new MySqlConnection(conex.CxSQL()))
            {
                conex.OpenConection(conexion);
                using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_obtener_listaDescuentoxlistaCategoria", conexion))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new MySqlParameter("p_xmlCategoria", MySqlDbType.String)).Value = xmlCategoria;
                    command.Parameters.Add(new MySqlParameter("p_fecha", MySqlDbType.VarChar)).Value = Fecha;
                    command.Parameters.Add(new MySqlParameter("p_hora", MySqlDbType.VarChar)).Value = hora;
                    command.CommandTimeout = 0;
                    dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CENPorDescTarj.Descripcion = dr["l_xml_descuentos"].ToString().Trim();
                        }
                    }
                }

            }
            return CENPorDescTarj;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conex.CerrarConexion(conexion);
        }
    }

    public CENPorDescTarj CADListaDescuentoxListCategycodBIN(string xmlCategoria, string codbin,string Fecha, string hora)
    {

        CENPorDescTarj CENPorDescTarj = new CENPorDescTarj();
        MySqlDataReader dr;
        CADConexion conex = new CADConexion();


        try
        {
            DateTime fech = DateTime.Parse(Fecha);
            Fecha = fech.ToString("yyyy-MM-dd");


            using (conexion = new MySqlConnection(conex.CxSQL()))
            {
                conex.OpenConection(conexion);
                using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_obtener_listDsctoxlistCategycodbin", conexion))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new MySqlParameter("p_xmlCategoria", MySqlDbType.String)).Value = xmlCategoria;
                    command.Parameters.Add(new MySqlParameter("p_codigo_BIN", MySqlDbType.String)).Value = codbin;
                    command.Parameters.Add(new MySqlParameter("p_fecha", MySqlDbType.VarChar)).Value = Fecha;
                    command.Parameters.Add(new MySqlParameter("p_hora", MySqlDbType.VarChar)).Value = hora;
                    command.CommandTimeout = 0;
                    dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            CENPorDescTarj.Descripcion = dr["l_xml_descuentos"].ToString().Trim();
                        }
                    }
                }

            }
            return CENPorDescTarj;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conex.CerrarConexion(conexion);
        }
    }

    public Int16 CADCantidadDescuentoxListCategoria(string xmlCategoria, string Fecha, string hora)
    {

        Int16 cantDscto = 0;
        MySqlDataReader dr;
        CADConexion conex = new CADConexion();


        try
        {
            DateTime fech = DateTime.Parse(Fecha);
            Fecha = fech.ToString("yyyy-MM-dd");


            using (conexion = new MySqlConnection(conex.CxSQL()))
            {
                conex.OpenConection(conexion);
                using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_obtenerCantDsctos_xlistCategorias", conexion))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new MySqlParameter("p_xmlCategoria", MySqlDbType.String)).Value = xmlCategoria;
                    command.Parameters.Add(new MySqlParameter("p_fecha", MySqlDbType.VarChar)).Value = Fecha;
                    command.Parameters.Add(new MySqlParameter("p_hora", MySqlDbType.VarChar)).Value = hora;
                    command.CommandTimeout = 0;
                    dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            cantDscto = Convert.ToInt16(dr["l_cantdsct"].ToString());
                        }
                    }
                }

            }
            return cantDscto;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            conex.CerrarConexion(conexion);
        }
    }
}