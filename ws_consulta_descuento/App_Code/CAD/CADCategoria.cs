﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient;


/// <summary>
/// Descripción breve de CADCategoria
/// </summary>
public class CADCategoria
{

    private MySqlConnection conexion;
    public CENResponseCategoria Categoria(List<CENCategoria> lista, string ip)
    {

        CENResponseCategoria responseCategoria = new CENResponseCategoria();
        MySqlDataReader dr = null;
        CADConexion conex = new CADConexion();

        conexion = new MySqlConnection(conex.CxSQL());
        conex.OpenConection(conexion);
        MySqlTransaction trans = conexion.BeginTransaction();
        try
        {
            //baja Categoria
            using (MySqlCommand command2 = new MySqlCommand("pa_dbcanasteria_vt_BajaCategoria", conexion))
            {
                command2.CommandType = CommandType.StoredProcedure;
                command2.CommandTimeout = 0;
                dr = command2.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        responseCategoria.Respuesta = Convert.ToInt32(dr["codigo"].ToString());
                        responseCategoria.Descripcion = dr["descripcion"].ToString();
                    }
                }
            }

            dr.Close();
            //

            if (responseCategoria.Respuesta == 0)
            {
                responseCategoria.Respuesta = 0;
                responseCategoria.Descripcion = "";
                foreach (CENCategoria datos in lista)
                {
                    if (datos.id > 0)

                    {
                        using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_categoria", conexion))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new MySqlParameter("p_codcat", MySqlDbType.Int32)).Value = datos.id;
                            command.Parameters.Add(new MySqlParameter("p_descat", MySqlDbType.VarChar)).Value = datos.name;
                            command.Parameters.Add(new MySqlParameter("p_ip", MySqlDbType.VarChar)).Value = ip;
                            command.CommandTimeout = 0;
                            dr = command.ExecuteReader();
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    responseCategoria.Respuesta = Convert.ToInt32(dr["codigo"].ToString());
                                    responseCategoria.Descripcion = dr["descripcion"].ToString();

                                    if (responseCategoria.Respuesta <0)
                                    {
                                        dr.Close();
                                        trans.Rollback();
                                        return responseCategoria;
                                    }
                                }
                            }
                        }

                    }
                    dr.Close();
                }
            }
            else
            {
                trans.Rollback();
                return responseCategoria;
            }


            trans.Commit();
            return responseCategoria;
        }
        catch (Exception ex)
        {
            trans.Rollback();
            throw ex;
        }
        finally

        {
            conex.CerrarConexion(conexion);
            dr.Close();
        }
    }
}