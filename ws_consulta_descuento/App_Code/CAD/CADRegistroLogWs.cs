﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;

/// <summary>
/// Descripción breve de CADRegistroLogWs
/// </summary>
public class CADRegistroLogWs
{
    private MySqlConnection conexion;
   

    public bool RegistroLogWs(string direurl , string webserv, Int16 tipolog , Int16 flagerr, Int16 origerr, string logcerr, string descerr, 
                             string datentr, string datsali, string ip)
    {

        int Codigo = 0;
         MySqlDataReader dr;
        CADConexion conex = new CADConexion();

        try
        {
            using (conexion = new MySqlConnection(conex.CxSQL()))
            {
                conex.OpenConection(conexion);
                using (MySqlCommand command = new MySqlCommand("pa_dbcanasteria_vt_registrar_logws", conexion))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new MySqlParameter("p_direurl", MySqlDbType.VarChar)).Value = direurl;
                    command.Parameters.Add(new MySqlParameter("p_webserv", MySqlDbType.VarChar)).Value = webserv;
                    command.Parameters.Add(new MySqlParameter("p_tipolog", MySqlDbType.Int16)).Value = tipolog;
                    command.Parameters.Add(new MySqlParameter("p_flagerr", MySqlDbType.Int16)).Value = flagerr;
                    command.Parameters.Add(new MySqlParameter("p_origerr", MySqlDbType.Int16)).Value = origerr;
                    command.Parameters.Add(new MySqlParameter("p_logcerr", MySqlDbType.VarChar)).Value = logcerr;
                    command.Parameters.Add(new MySqlParameter("p_descerr", MySqlDbType.String)).Value = descerr;
                    command.Parameters.Add(new MySqlParameter("p_datentr", MySqlDbType.String)).Value = datentr;
                    command.Parameters.Add(new MySqlParameter("p_datsali", MySqlDbType.String)).Value = datsali;
                    command.Parameters.Add(new MySqlParameter("p_ip", MySqlDbType.VarChar)).Value = ip;
                    command.CommandTimeout = 0;
                    dr = command.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Codigo = Convert.ToInt32(dr["last"]);
                        }
                    }
                }

            }
            return true;
        
        }
        catch (Exception )
        {
            return false;
        }

        finally
        {
            conex.CerrarConexion(conexion);
        }

    }

}