﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

/// <summary>
/// Descripción breve de WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    public CENResponseDescuento ConsultaDescuento(string nuntarjeta, string categoria, string fecha, string hora)
    {
        Int16 flag = 0; //flag validar datos
        string strUrlWebService = "";
        string webserv = "";
        short shrTipoSer = 0;
        string strDatosSal = "";
        string strDatosEnv = "";
        Int16 flagerr = 0;
        String nommetodo = "ConsultaDescuento";
        CENResponseDescuento cenresponse = new CENResponseDescuento();
        CENPorDescTarj objCENPorDescTarj = new CENPorDescTarj();
        CENErrWs objerrdesc = new CENErrWs();

        //registro log
        String localIP = "";


        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());// objeto para guardar la ip
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                localIP = ip.ToString();// esta es nuestra ip
            }
        }


        try
        {
            CLNPorDescTarj ConsultarDescuento = new CLNPorDescTarj();

            //validación Datos
            flag = ConsultarDescuento.ValidarDatos(nuntarjeta, categoria, fecha, hora);

            //
            if (flag == 0)
            {
                cenresponse.Descuento = ConsultarDescuento.ConsultarDescuento(nuntarjeta, categoria, fecha, hora);
                objerrdesc.CodigoErr = 2000;
                objerrdesc.DescripcionErr = "EXITO";
                objerrdesc.TipoErr = 0;
                cenresponse.Errws = objerrdesc;
                flagerr = 0;
            }
            else
            {
                objerrdesc.DescripcionErr = "Por favor verifique los datos ingresados...";
                cenresponse.Descuento = objCENPorDescTarj;
                objerrdesc.CodigoErr = 3000;
                objerrdesc.TipoErr = 2;
                cenresponse.Errws = objerrdesc;
                flagerr = 2;
            }

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "NumTarjeta: " + nuntarjeta + "|" + "Fecha: " + fecha + "|" + "HORA: " + hora;
            strDatosSal = "Descripcion Descuento: " + cenresponse.Descuento.Descripcion + " Codigo Descuento:" + cenresponse.Descuento.Codigo +
                " Porcentaje Descuento:" + cenresponse.Descuento.Porcentaje + "|" + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";


        }
        catch (Exception ex)
        {
            cenresponse.Descuento = objCENPorDescTarj;
            objerrdesc.CodigoErr = 4000;
            objerrdesc.DescripcionErr = "ERROR:" + ex.Message + " - TRACE: " + ex.StackTrace;
            objerrdesc.TipoErr = 1;
            cenresponse.Errws = objerrdesc;
            flagerr = 1;

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "NumTarjeta: " + nuntarjeta + "|" + "Fecha: " + fecha + "|" + "HORA: " + hora;
            strDatosSal = "Descripcion Descuento: " + cenresponse.Descuento.Descripcion + " Codigo Descuento:" + cenresponse.Descuento.Codigo +
                " Porcentaje Descuento:" + cenresponse.Descuento.Porcentaje + "|" + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";

        }

        return cenresponse;
    }


    [WebMethod]
    public CENResponseCategoria2 Categorias()
    {
        string strUrlWebService = "";
        string webserv = "";
        short shrTipoSer = 0;
        string strDatosSal = "";
        string strDatosEnv = "";
        Int16 flagerr = 0;
        String nommetodo = "Categorias";

        CENResponseCategoria CENResponseCategoria = new CENResponseCategoria();
        CLNCategoria CLNCategoria = new CLNCategoria();

        CENResponseCategoria2 cenresponsec = new CENResponseCategoria2();
        CENErrWs objerrws = new CENErrWs();

        //obteniendo ip
        String localIP = "";
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());// objeto para guardar la ip
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                localIP = ip.ToString();// esta es nuestra ip
            }
        }
        //
        try
        {
            string credenciales = "";
            object resp = new object();

            string URLWebAPI = ConfigurationManager.AppSettings.GetValues("URLWebAPI").GetValue(0).ToString().Trim();
            string username = ConfigurationManager.AppSettings.GetValues("username").GetValue(0).ToString().Trim();
            string password = ConfigurationManager.AppSettings.GetValues("password").GetValue(0).ToString().Trim();

            credenciales = username + ":" + password;

           

            // Crear un objeto HttpClient para acceder a ,la Web API
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URLWebAPI);

            //Agregar campos de seguridad basica
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                        Convert.ToBase64String(Encoding.Default.GetBytes(credenciales)));

            // Especificar que estamos aceptando datos JSON
            client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.
                MediaTypeWithQualityHeaderValue("application/json"));

            // Serializar los datos a formato JSON
            //string DataJSON = JsonConvert.SerializeObject("");

            // Obtener un tipo HttpContent para pasarlo en la petición 
            //StringContent Content = new StringContent(DataJSON,System.Text.Encoding.UTF8, "application/json");

            // Realizar la llamada al recurso Web API y obtener la respuesta
            HttpResponseMessage response = client.GetAsync(URLWebAPI).Result;

            List<CENCategoria> lista = new List<CENCategoria>();
            listaDatos lDatos = new listaDatos();
            if (response.IsSuccessStatusCode)
            {
                // Deserializar el resultado obtenido
                lista = JsonConvert.DeserializeObject<List<CENCategoria>>(
                            response.Content.ReadAsStringAsync().Result);

                cenresponsec.categoria = CLNCategoria.Categoria(lista, localIP);
                if (cenresponsec.categoria.Respuesta==0)
                    {
                    objerrws.CodigoErr = 2000;
                    objerrws.DescripcionErr = "EXITO";
                    objerrws.TipoErr = 0;
                }
                else
                {
                    objerrws.CodigoErr = 3000;
                    objerrws.DescripcionErr = "ERROR EN BD";
                    objerrws.TipoErr = 2;
                    flagerr = 2;
                }
                cenresponsec.Errws = objerrws;
            }

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "";
            strDatosSal = "Codigo Respuesta: " + cenresponsec.categoria.Respuesta + " Descripcion:" + cenresponsec.categoria.Descripcion +
                "DatosRespuestaErr Codigo: " + cenresponsec.Errws.CodigoErr +
                " Descripcion Error: " + cenresponsec.Errws.DescripcionErr + " Tipo de Error:" + cenresponsec.Errws.TipoErr;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrws.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";

        }
        catch(Exception ex)
        {
            cenresponsec.categoria = CENResponseCategoria;
            objerrws.CodigoErr = 4000;
            objerrws.DescripcionErr = "ERROR:" + ex.Message + " - TRACE: " + ex.StackTrace;
            objerrws.TipoErr = 1;
            flagerr = 1;
            cenresponsec.Errws = objerrws;

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "";
            strDatosSal = "Codigo Respuesta: " + cenresponsec.categoria.Respuesta + " Descripcion:" + cenresponsec.categoria.Descripcion +
                "DatosRespuestaErr Codigo: " + cenresponsec.Errws.CodigoErr +
                " Descripcion Error: " + cenresponsec.Errws.DescripcionErr + " Tipo de Error:" + cenresponsec.Errws.TipoErr;

             CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrws.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";
        }

       
        return cenresponsec;
    }


    [WebMethod]
    public CENResponsexmlDescuentos ListDescuentoxListaCategoria(string xmlCategoria, string Fecha, string hora)
    {
        string strUrlWebService = "";
        string webserv = "";
        short shrTipoSer = 0;
        string strDatosSal = "";
        string strDatosEnv = "";
        Int16 flagerr = 0;
        String nommetodo = "ListDescuentoxListaCategoria";
        CENResponsexmlDescuentos cenresponse = new CENResponsexmlDescuentos();
        CENPorDescTarj objCENPorDescTarj = new CENPorDescTarj();
        CENErrWs objerrdesc = new CENErrWs();

        //registro log
        String localIP = "";


        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());// objeto para guardar la ip
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                localIP = ip.ToString();// esta es nuestra ip
            }
        }


        try
        {
            CLNPorDescTarj ConsultarDescuento = new CLNPorDescTarj();

            cenresponse.Descuento = ConsultarDescuento.CLNListaDescuentoxListaCategoria(xmlCategoria, Fecha, hora).Descripcion;
            objerrdesc.CodigoErr = 2000;
            objerrdesc.DescripcionErr = "EXITO";
            objerrdesc.TipoErr = 0;
            cenresponse.Errws = objerrdesc;
            flagerr = 0;
           

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "XML_Categoria: " + xmlCategoria + "|" + "Fecha: " + Fecha + "|" + "HORA: " + hora;
            strDatosSal = "XML_Descuento: " + cenresponse.Descuento +"DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr; ;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";


        }
        catch (Exception ex)
        {
            cenresponse.Descuento = "";
            objerrdesc.CodigoErr = 4000;
            objerrdesc.DescripcionErr = "ERROR:" + ex.Message + " - TRACE: " + ex.StackTrace;
            objerrdesc.TipoErr = 1;
            cenresponse.Errws = objerrdesc;
            flagerr = 1;

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "NumTarjeta: " + xmlCategoria + "|" + "Fecha: " + Fecha + "|" + "HORA: " + hora;
            strDatosSal = "XML_Descuentos: " + cenresponse.Descuento + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr; ;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";

        }

        return cenresponse;
    }

    [WebMethod]
    public CENResponsexmlDescuentos ListDescuentoxListaCategoriayCodBIN(string xmlCategoria,string codbin, string Fecha, string hora)
    {
        string strUrlWebService = "";
        string webserv = "";
        short shrTipoSer = 0;
        string strDatosSal = "";
        string strDatosEnv = "";
        Int16 flagerr = 0;
        String nommetodo = "ListDescuentoxListaCategoriayCodBIN";
        CENResponsexmlDescuentos cenresponse = new CENResponsexmlDescuentos();
        CENPorDescTarj objCENPorDescTarj = new CENPorDescTarj();
        CENErrWs objerrdesc = new CENErrWs();

        //registro log
        String localIP = "";


        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());// objeto para guardar la ip
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                localIP = ip.ToString();// esta es nuestra ip
            }
        }


        try
        {
            CLNPorDescTarj ConsultarDescuento = new CLNPorDescTarj();

            cenresponse.Descuento = ConsultarDescuento.CLNListaDescuentoxListaCategoriaycodbin(xmlCategoria, codbin,Fecha, hora).Descripcion;
            objerrdesc.CodigoErr = 2000;
            objerrdesc.DescripcionErr = "EXITO";
            objerrdesc.TipoErr = 0;
            cenresponse.Errws = objerrdesc;
            flagerr = 0;


            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "XML_Categoria: " + xmlCategoria + "|" +" Codigo BIN:"+codbin+ "|" + " Fecha: " + Fecha + "|" + " HORA: " + hora;
            strDatosSal = "XML_Descuento: " + cenresponse.Descuento + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr; ;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";


        }
        catch (Exception ex)
        {
            cenresponse.Descuento = "";
            objerrdesc.CodigoErr = 4000;
            objerrdesc.DescripcionErr = "ERROR:" + ex.Message + " - TRACE: " + ex.StackTrace;
            objerrdesc.TipoErr = 1;
            cenresponse.Errws = objerrdesc;
            flagerr = 1;

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "NumTarjeta: " + xmlCategoria + "|" + "Codigo BIN:" + codbin+ "|" + "Fecha: " + Fecha + "|" + "HORA: " + hora;
            strDatosSal = "XML_Descuentos: " + cenresponse.Descuento + "|" + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr; ;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";

        }

        return cenresponse;
    }

    [WebMethod]
    public CENResponseCantDescuentoxListaCategoria CantidadDescuentoxListCategoria(string xmlCategoria, string Fecha, string hora)
    {
        string strUrlWebService = "";
        string webserv = "";
        short shrTipoSer = 0;
        string strDatosSal = "";
        string strDatosEnv = "";
        Int16 flagerr = 0;
        String nommetodo = "CantidadDescuentoxListCategoria";
        CENResponseCantDescuentoxListaCategoria cenresponse = new CENResponseCantDescuentoxListaCategoria();
        CENErrWs objerrdesc = new CENErrWs();

        //registro log
        String localIP = "";


        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());// objeto para guardar la ip
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily.ToString() == "InterNetwork")
            {
                localIP = ip.ToString();// esta es nuestra ip
            }
        }


        try
        {
            CLNPorDescTarj ConsultarDescuento = new CLNPorDescTarj();

            cenresponse.cantDsto = ConsultarDescuento.CLNCantidadDescuentoxListCategoria(xmlCategoria, Fecha, hora);
            objerrdesc.CodigoErr = 2000;
            objerrdesc.DescripcionErr = "EXITO";
            objerrdesc.TipoErr = 0;
            cenresponse.Errws = objerrdesc;
            flagerr = 0;


            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "XML_Categoria: " + xmlCategoria + "|" + " Fecha: " + Fecha + "|" + " HORA: " + hora;
            strDatosSal = "XML_Descuento: " + cenresponse.cantDsto + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr; ;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";


        }
        catch (Exception ex)
        {
            cenresponse.cantDsto = 0;
            objerrdesc.CodigoErr = 4000;
            objerrdesc.DescripcionErr = "ERROR:" + ex.Message + " - TRACE: " + ex.StackTrace;
            objerrdesc.TipoErr = 1;
            cenresponse.Errws = objerrdesc;
            flagerr = 1;

            CADRegistroLogWs CADRegistroLogWs = new CADRegistroLogWs();
            strUrlWebService = ConfigurationManager.AppSettings.GetValues("URL_WEB_SERVICE").GetValue(0).ToString().Trim();
            shrTipoSer = Convert.ToInt16(ConfigurationManager.AppSettings.GetValues("TIPO_SERVIDOR").GetValue(0).ToString());
            webserv = ConfigurationManager.AppSettings.GetValues("WebService").GetValue(0).ToString().Trim();
            strDatosEnv = "NumTarjeta: " + xmlCategoria + "|" +  "Fecha: " + Fecha + "|" + "HORA: " + hora;
            strDatosSal = "XML_Descuentos: " + cenresponse.cantDsto + "|" + "DatosRespuestaErr Codigo: " + cenresponse.Errws.CodigoErr +
                " Descripcion Error: " + cenresponse.Errws.DescripcionErr + " Tipo de Error:" + cenresponse.Errws.TipoErr; ;

            CADRegistroLogWs.RegistroLogWs(strUrlWebService.Trim(), webserv, shrTipoSer, flagerr, 1, nommetodo, objerrdesc.DescripcionErr.Trim(),
                strDatosEnv.Trim(), strDatosSal, localIP);
            strDatosEnv = "";
            strDatosSal = "";

        }

        return cenresponse;
    }


}
