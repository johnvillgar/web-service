﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

/// <summary>
/// Descripción breve de CLNPorDescTarj
/// </summary>
public class CLNPorDescTarj
{
    public CENPorDescTarj ConsultarDescuento(string CodigoBin, string categoria, string Fecha, string hora)
    {
        CADPorDescTarj CADPorDescTarj = new CADPorDescTarj();
        CENPorDescTarj CENPorDescTarj = new CENPorDescTarj();

        try
        {
            CENPorDescTarj = CADPorDescTarj.consultarDescuento(CodigoBin, categoria, Fecha, hora);

            return CENPorDescTarj;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public Int16 ValidarDatos(string CodigoBin, string categoria,string Fecha, string hora)
    {
        //validando datos obligatorios
        if (string.IsNullOrWhiteSpace(CodigoBin) || string.IsNullOrWhiteSpace(Fecha) || string.IsNullOrWhiteSpace(hora)
            || string.IsNullOrWhiteSpace(Convert.ToString(categoria)))
        {
            return 1;
        }
        else
        {
            CodigoBin = CodigoBin.Trim();
            categoria = categoria.Trim();
            Fecha = Fecha.Trim();
            hora = hora.Trim();
        }

        //validando categoria
        Regex reg = new Regex("[0-9]");//Regex
        for (int i = 0; i < categoria.Length; i++)
        {
            if (!(reg.IsMatch(categoria.Substring(i, 1))))
            {
                return 1;
            }
        }

        if (Convert.ToInt32(categoria) < 0)
        {
            return 2;
        }
        //validando formato de fecha
        if (!ValidarFecha(Fecha.Trim()))
        //#(@#)1-A Fin
        {
            return 3;
        }


       return 0;

    }

    public bool ValidarFecha(string fecha)
    {
        //DESCRIPCIÓN: Validar fecha
        try
        {
            CultureInfo culture = new CultureInfo("es-PE");//Cultureinfo
            Convert.ToDateTime(fecha, culture);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public CENPorDescTarj CLNListaDescuentoxListaCategoria(string xmlCategoria, string Fecha, string hora)
    {
        CADPorDescTarj CADPorDescTarj = new CADPorDescTarj();
        CENPorDescTarj CENPorDescTarj = new CENPorDescTarj();

        try
        {
            CENPorDescTarj = CADPorDescTarj.CADListaDescuentoxListCateg(xmlCategoria, Fecha, hora);

            return CENPorDescTarj;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public CENPorDescTarj CLNListaDescuentoxListaCategoriaycodbin(string xmlCategoria, string codbin,string Fecha, string hora)
    {
        CADPorDescTarj CADPorDescTarj = new CADPorDescTarj();
        CENPorDescTarj CENPorDescTarj = new CENPorDescTarj();

        try
        {
            CENPorDescTarj = CADPorDescTarj.CADListaDescuentoxListCategycodBIN(xmlCategoria, codbin,Fecha, hora);

            return CENPorDescTarj;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public Int16 CLNCantidadDescuentoxListCategoria(string xmlCategoria, string Fecha, string hora)
    {
        CADPorDescTarj CADPorDescTarj = new CADPorDescTarj();
        Int16 CantDscto = 0 ;

        try
        {
            CantDscto = CADPorDescTarj.CADCantidadDescuentoxListCategoria(xmlCategoria, Fecha, hora);

            return CantDscto;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}