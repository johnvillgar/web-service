﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CLNCategoria
/// </summary>
public class CLNCategoria
{
    public CENResponseCategoria Categoria(List<CENCategoria> lista, string ip)
    {
        CADCategoria CACategoria = new CADCategoria();
        CENResponseCategoria responseCategoria = new CENResponseCategoria();

        try
        {
             responseCategoria = CACategoria.Categoria(lista, ip);

            return responseCategoria;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}